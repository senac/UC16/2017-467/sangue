package br.com.senac.projetofinal.servlet;

import br.com.senac.projetofinal.dao.UsuarioDAO;
import br.com.senac.projetofinal.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest requisicao, HttpServletResponse resposta) throws ServletException, IOException {

        String usuario = requisicao.getParameter("user");
        String navegador = requisicao.getParameter("User-Agent");

        resposta.setContentType("text/html");
        PrintWriter out = resposta.getWriter();

        out.println("<html>");
        out.println("<body>");
        out.println("Olá , " + usuario + "</b><br/>");
        out.println("Seja Bem-Vindo ao Site Doe mais Saúde!!!!!");
        out.println("Você esta usando o navegador: " + navegador);
        out.println("</body>");
        out.println("</html>");

    }

    @Override
    protected void doPost(HttpServletRequest requisicao, HttpServletResponse resposta) throws ServletException, IOException {

        String email = requisicao.getParameter("email");
        String senha = requisicao.getParameter("senha");

        PrintWriter out = resposta.getWriter();

        out.println("<body>");
        out.println("<html>");

        if ((email != null && senha != null) && (!email.trim().isEmpty() && !senha.trim().isEmpty())) {

            UsuarioDAO dao = new UsuarioDAO();
            Usuario usuario = dao.getByname(email);

            if (usuario != null && usuario.getSenha().equals(senha)) {

                HttpSession session = requisicao.getSession();
                session.setAttribute("user", usuario);
               

                resposta.sendRedirect("usuarioLogado/homeLogado.jsp");

            } else {
                if (usuario == null) {
                    out.println("Usuário Inexistente!!!!");
                } else {

                    out.println("Falha na autenticação");

                }

            }
            out.println("</body>");
            out.println("</html>");

        } else {

            resposta.sendRedirect("login.jsp");

        }
        /*
        Usuario usuario = new Usuario();

        // PrintWriter out = resposta.getWriter();
        out.println("<html>");
        out.println("<body>");

        if (usuario.getEmail().equals("Zidane") && usuario.getSenha().equals("123")) {

        } else {
            out.println("Falha na autenticação");
        }
        out.println("</body>");
        out.println("</html>"); */

    }

}
