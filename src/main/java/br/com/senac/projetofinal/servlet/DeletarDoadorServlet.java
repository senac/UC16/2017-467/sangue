package br.com.senac.projetofinal.servlet;

import br.com.senac.projetofinal.dao.DoadorDAO;
import br.com.senac.projetofinal.model.Doador;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "DeletarDoadorServlet", urlPatterns = {"/usuarioLogado/DeletarDoadorServlet"})
public class DeletarDoadorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        String formId = request.getParameter("formId");
        String formNome = request.getParameter("formNome");
        String formEstado = request.getParameter("formEstado");
        String formSangue = request.getParameter("formSangue");

        String erro = null;

        try {
            Integer codigo = null;
            if (id != null && !id.trim().isEmpty()) {
                codigo = Integer.parseInt(id);
            }

            Integer idPesquisa = null;
            if (formId != null && !formId.trim().isEmpty()) {

                idPesquisa = Integer.parseInt(formId);
            }

            String nome = null;
            if (formNome != null && !formNome.trim().isEmpty()) {
                nome = formNome;
            }

            String estado = null;
            if (formEstado != null && !formEstado.trim().isEmpty()) {
                estado = formEstado;
            }

            String sangue = null;
            if (formSangue != null && !formSangue.trim().isEmpty()) {
                sangue = formSangue;
            }

            DoadorDAO dao = new DoadorDAO();

            Doador doador = new Doador();
            doador.setId(codigo);

            dao.deletar(doador);

            String mensagem = "Removido com sucesso";

            List<Doador> lista = dao.getByFiltro(idPesquisa, nome, estado, sangue);

            request.setAttribute("lista", lista);
            request.setAttribute("mensagem", mensagem);

        } catch (Exception ex) {
            erro = "Doador não encontrado";
            request.setAttribute("erro", erro);
            ex.printStackTrace();
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("pesquisarDoador.jsp");

        dispatcher.forward(request, response);
    }
}
