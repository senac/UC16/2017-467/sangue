package br.com.senac.projetofinal.servlet;

import br.com.senac.projetofinal.dao.PacienteDAO;
import br.com.senac.projetofinal.model.Paciente;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CadastrarPacienteServlet", urlPatterns = {"/usuarioLogado/CadastrarPacienteServlet"})
public class CadastrarPacienteServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Paciente paciente = null;
        String erro = null;
        String id = request.getParameter("id");

        try {
            int codigo;
            try {
                codigo = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                codigo = 0;
            }

            PacienteDAO dao = new PacienteDAO();

            paciente = dao.get(codigo);
            request.setAttribute("paciente", paciente);

        } catch (Exception ex) {
            erro = "Paciente não encontrado.";
            request.setAttribute("erro", erro);
            ex.printStackTrace();
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("cadastrarPaciente.jsp");

        dispatcher.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        String nome = request.getParameter("nome");
        String cpf = request.getParameter("cpf");
        String sexo = request.getParameter("sexo");
        String idade = request.getParameter("idade");
        String sangue = request.getParameter("sangue");
        String dataLimite = request.getParameter("dataLimite");
        String hospital = request.getParameter("hospital");
        String estado = request.getParameter("estado");
        String cidade = request.getParameter("cidade");
        String telefone = request.getParameter("telefone");
        String email = request.getParameter("email");
        String observacao = request.getParameter("observacao");

        String mensagem = null;
        String erro = null;

        try {
            Paciente paciente = new Paciente();
            int codigo;
            try {
                codigo = Integer.parseInt(id);

            } catch (NumberFormatException e) {
                codigo = 0;
            }

            paciente.setId(codigo);
            paciente.setNome(nome);
            paciente.setCpf(cpf);
            paciente.setSexo(sexo);
            paciente.setIdade(idade);
            paciente.setSangue(sangue);
            paciente.setDataLimite(dataLimite);
            paciente.setHospital(hospital);
            paciente.setEstado(estado);
            paciente.setCidade(cidade);
            paciente.setTelefone(telefone);
            paciente.setEmail(email);
            paciente.setObservacao(observacao);

            PacienteDAO dao = new PacienteDAO();

            dao.cadastrar(paciente);
            mensagem = "Salvo com sucesso!!!!!";
            request.setAttribute("paciente", paciente);
            request.setAttribute("mensagem", mensagem);

        } catch (Exception ex) {

            erro = "Erro ao salvar paciente.";
            request.setAttribute("erro", erro);
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("cadastrarPaciente.jsp");

        dispatcher.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
