package br.com.senac.projetofinal.servlet;

import br.com.senac.projetofinal.dao.DoadorDAO;
import br.com.senac.projetofinal.model.Doador;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CadastrarDoadorServlet", urlPatterns = {"/usuarioLogado/CadastrarDoadorServlet"})
public class CadastrarDoadorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Doador doador = null;
        String erro = null;
        String id = request.getParameter("id");

        try {
            int codigo;
            try {
                codigo = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                codigo = 0;
            }

            DoadorDAO dao = new DoadorDAO();

            doador = dao.get(codigo);
            request.setAttribute("doador", doador);
        } catch (Exception ex) {
            erro = "Doador não encontrado.";
            request.setAttribute("erro", erro);
            ex.printStackTrace();

        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("cadastrarDoador.jsp");

        dispatcher.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        String nome = request.getParameter("nome");
        String cpf = request.getParameter("cpf");
        String sexo = request.getParameter("sexo");
        String dataNascimento = request.getParameter("dataNascimento");
        String sangue = request.getParameter("sangue");
        String estado = request.getParameter("estado");
        String cidade = request.getParameter("cidade");
        String telefone = request.getParameter("telefone");
        String email = request.getParameter("email");

        String mensagem = null;
        String erro = null;

        try {
            Doador doador = new Doador();
            int codigo;
            try {
                codigo = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                codigo = 0;
            }

            doador.setId(codigo);
            doador.setNome(nome);
            doador.setCpf(cpf);
            doador.setSexo(sexo);
            doador.setDataNascimento(dataNascimento);
            doador.setSangue(sangue);
            doador.setEstado(estado);
            doador.setCidade(cidade);
            doador.setTelefone(telefone);
            doador.setEmail(email);

            DoadorDAO dao = new DoadorDAO();

            dao.cadastrar(doador);
            mensagem = "Salvo com sucesso!!!!";
            request.setAttribute("doador", doador);
            request.setAttribute("mensagem", mensagem);

        } catch (Exception ex) {
            erro = "Erro ao salvar doador.";
            request.setAttribute("erro", erro);
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("cadastrarDoador.jsp");

        dispatcher.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
