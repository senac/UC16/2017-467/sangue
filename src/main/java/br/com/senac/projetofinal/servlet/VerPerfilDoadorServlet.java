/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.projetofinal.servlet;

import br.com.senac.projetofinal.dao.DoadorDAO;
import br.com.senac.projetofinal.model.Doador;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sala304b
 */
@WebServlet(name = "VerPerfilDoadorServlet", urlPatterns = {"/usuarioLogado/VerPerfilDoadorServlet"})
public class VerPerfilDoadorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Doador doador = null;
        String erro = null;
        String id = request.getParameter("id");

        try {
            int codigo;
            try {
                codigo = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                codigo = 0;
            }

            DoadorDAO dao = new DoadorDAO();

            doador = dao.get(codigo);
            request.setAttribute("doador", doador);
        } catch (Exception ex) {
            erro = "Doador não encontrado.";
            request.setAttribute("erro", erro);
            ex.printStackTrace();

        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("verDoador.jsp");

        dispatcher.forward(request, response);

    }

}
