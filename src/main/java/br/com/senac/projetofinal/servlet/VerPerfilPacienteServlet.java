/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.projetofinal.servlet;

import br.com.senac.projetofinal.dao.PacienteDAO;
import br.com.senac.projetofinal.model.Paciente;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sala304b
 */
@WebServlet(name = "VerPerfilPacienteServlet", urlPatterns = {"/usuarioLogado/VerPerfilPacienteServlet"})
public class VerPerfilPacienteServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Paciente paciente = null;
        String erro = null;
        String id = request.getParameter("id");

        try {
            int codigo;
            try {
                codigo = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                codigo = 0;
            }

            PacienteDAO dao = new PacienteDAO();

            paciente = dao.get(codigo);
            request.setAttribute("paciente", paciente);

        } catch (Exception ex) {
            erro = "Paciente não encontrado.";
            request.setAttribute("erro", erro);
            ex.printStackTrace();
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("verPaciente.jsp");

        dispatcher.forward(request, response);

    }


}
