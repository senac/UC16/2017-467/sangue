package br.com.senac.projetofinal.servlet;

import br.com.senac.projetofinal.dao.UsuarioDAO;
import br.com.senac.projetofinal.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CadastrarUsuarioServlet", urlPatterns = {"/CadastrarUsuarioServlet"})
public class CadastrarUsuarioServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Usuario usuario = null;
        String erro = null;
        String id = request.getParameter("id");

        try {

            int codigo;
            try {
                codigo = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                codigo = 0;
            }

            UsuarioDAO dao = new UsuarioDAO();

            usuario = dao.get(codigo);
            request.setAttribute("usuario", usuario);

        } catch (Exception ex) {
            erro = "Usuario não encontrado.";
            request.setAttribute("erro", erro);
            ex.printStackTrace();
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("cadastroUsuario.jsp");

        dispatcher.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        String nome = request.getParameter("nome");
        String sexo = request.getParameter("sexo");
        String dataNascimento = request.getParameter("dataNascimento");
        String sangue = request.getParameter("sangue");
        String estado = request.getParameter("estado");
        String cidade = request.getParameter("cidade");
        String bairro = request.getParameter("bairro");
        String endereco = request.getParameter("endereco");
        String telefone = request.getParameter("telefone");
        String email = request.getParameter("email");
        String senha = request.getParameter("senha");

        String mensagem = null;
        String erro = null;

        try {

            Usuario usuario = new Usuario();
            int codigo;
            try {
                codigo = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                codigo = 0;
            }

            usuario.setId(codigo);
            usuario.setNome(nome);
            usuario.setSexo(sexo);
            usuario.setDataNascimento(dataNascimento);
            usuario.setSangue(sangue);
            usuario.setEstado(estado);
            usuario.setCidade(cidade);
            usuario.setBairro(bairro);
            usuario.setEndereco(endereco);
            usuario.setTelefone(telefone);
            usuario.setEmail(email);
            usuario.setSenha(senha);

            UsuarioDAO dao = new UsuarioDAO();

            dao.cadastrar(usuario);
            mensagem = "Salvo com sucesso!!!";
            request.setAttribute("usuario", usuario);
            request.setAttribute("mensagem", mensagem);

        } catch (Exception ex) {
            erro = "Erro ao salvar usuario.";
            request.setAttribute("erro", erro);
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("cadastroUsuario.jsp");

        dispatcher.forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
