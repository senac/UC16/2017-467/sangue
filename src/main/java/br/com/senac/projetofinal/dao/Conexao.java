package br.com.senac.projetofinal.dao;

import java.sql.*;

public class Conexao {

    private static final String URL = "jdbc:mysql://localhost:3306/projetofinal";
    private static final String USER = "root";
    private static final String PASSWORD = "";
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    public static Connection getConnection() {

        Connection connection = null;

        try {

            Class.forName(DRIVER);

            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Conectado");
        } catch (ClassNotFoundException ex) {

            System.out.println("Não foi possivel carregar o driver.");
        } catch (SQLException ex) {

            System.out.println("Não foi possivel conectar ao banco.");
        }
        return connection;
    }

    public static void main(String[] args) {
        Conexao.getConnection();
    }

}
