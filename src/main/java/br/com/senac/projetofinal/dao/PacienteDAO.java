package br.com.senac.projetofinal.dao;

import br.com.senac.projetofinal.model.Paciente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PacienteDAO extends DAO<Paciente> {

    @Override
    public void cadastrar(Paciente paciente) {

        Connection connection = null;
        try {
            String query;
            if (paciente.getId() == 0) {

                query = "insert into paciente (nome, cpf, sexo, idade, sangue, dataLimite, hospital, estado, cidade, telefone, email, observacao) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

            } else {
                query = "update paciente set nome = ?, cpf = ?, sexo = ?, idade = ?, sangue = ?, dataLimite = ?, hospital = ?, estado = ?, cidade = ?, telefone = ?, email = ?, observacao = ? where id = ? ;";

            }

            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, paciente.getNome());
            ps.setString(2, paciente.getCpf());
            ps.setString(3, paciente.getSexo());
            ps.setString(4, paciente.getIdade());
            ps.setString(5, paciente.getSangue());
            ps.setString(6, paciente.getDataLimite());
            ps.setString(7, paciente.getHospital());
            ps.setString(8, paciente.getEstado());
            ps.setString(9, paciente.getCidade());
            ps.setString(10, paciente.getTelefone());
            ps.setString(11, paciente.getEmail());
            ps.setString(12, paciente.getObservacao());
            if (paciente.getId() == 0) {
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                paciente.setId(rs.getInt(1));
            } else {
                ps.setInt(13, paciente.getId());
                ps.executeUpdate();

            }

        } catch (Exception ex) {

        } finally {

            try {
                connection.close();

            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão......");

            }
        }

    }

    @Override
    public void deletar(Paciente paciente) {

        String query = "delete from paciente where id = ? ";
        Connection connection = null;
        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, paciente.getId());
            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println("Erro ao deletar registro....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão....");
            }
        }

    }

    @Override
    public List<Paciente> listar() {

        String query = "select * from paciente;";
        List<Paciente> lista = new ArrayList<>();
        Connection connection = null;

        try {
            connection = Conexao.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Paciente paciente = new Paciente();
                paciente.setId(rs.getInt("id"));
                paciente.setNome(rs.getString("nome"));
                paciente.setCpf(rs.getString("cpf"));
                paciente.setSexo(rs.getString("sexo"));
                paciente.setIdade(rs.getString("idade"));
                paciente.setSangue(rs.getString("sangue"));
                paciente.setDataLimite(rs.getString("dataLimite"));
                paciente.setHospital(rs.getString("hospital"));
                paciente.setEstado(rs.getString("estado"));
                paciente.setCidade(rs.getString("cidade"));
                paciente.setTelefone(rs.getString("telefone"));
                paciente.setEmail(rs.getString("email"));
                paciente.setObservacao(rs.getString("observacao"));

                lista.add(paciente);
            }
        } catch (Exception ex) {
            System.out.println("Ocorreu um erro ao fazer a consulta.....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão.....");

            }
        }
        return lista;
    }

    @Override
    public Paciente get(int id) {

        Paciente paciente = null;
        Connection connection = null;
        String query = "select * from paciente where id = ? ; ";

        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {

                paciente = new Paciente();
                
                paciente.setId(rs.getInt("id"));
                paciente.setNome(rs.getString("nome"));
                paciente.setCpf(rs.getString("cpf"));
                paciente.setSexo(rs.getString("sexo"));
                paciente.setIdade(rs.getString("idade"));
                paciente.setSangue(rs.getString("sangue"));
                paciente.setDataLimite(rs.getString("dataLimite"));
                paciente.setHospital(rs.getString("hospital"));
                paciente.setEstado(rs.getString("estado"));
                paciente.setCidade(rs.getString("cidade"));
                paciente.setTelefone(rs.getString("telefone"));
                paciente.setEmail(rs.getString("email"));
                paciente.setObservacao(rs.getString("observacao"));

            }

        } catch (Exception ex) {
            System.out.println("Erro ao executar a consulta....");

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falaha ao fechar conexão...");
            }
        }
        return paciente;

    }

    public List<Paciente> getByFiltro(Integer id, String nome, String estado, String sangue) {

        List<Paciente> lista = new ArrayList<>();
        Connection connection = null;

        try {

            StringBuilder sb = new StringBuilder("select * from paciente where 1 = 1");

            if (id != null) {
                sb.append(" and id  = ? ");

            }

            if (nome != null && !nome.trim().isEmpty()) {
                sb.append(" and nome like ? ");

            }

            if (estado != null && !estado.trim().isEmpty()) {
                sb.append(" and estado like ? ");
            }

            if (sangue != null && !sangue.trim().isEmpty()) {
                sb.append(" and sangue like ? ");

            }
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(sb.toString());
            int index = 0;
            if (id != null) {
                ps.setInt(++index, id);
            }

            if (nome != null && !nome.trim().isEmpty()) {
                ps.setString(++index, "%" + nome + "%");
            }
            if (estado != null && !estado.trim().isEmpty()) {
                ps.setString(++index, "%" + estado + "%");
            }

            if (sangue != null && !sangue.trim().isEmpty()) {
                ps.setString(++index, "%" + sangue + "%");
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Paciente paciente = new Paciente();
                paciente.setId(rs.getInt("id"));
                paciente.setNome(rs.getString("nome"));
                paciente.setCpf(rs.getString("cpf"));
                paciente.setSexo(rs.getString("sexo"));
                paciente.setIdade(rs.getString("idade"));
                paciente.setSangue(rs.getString("sangue"));
                paciente.setDataLimite(rs.getString("dataLimite"));
                paciente.setHospital(rs.getString("hospital"));
                paciente.setEstado(rs.getString("estado"));
                paciente.setCidade(rs.getString("cidade"));
                paciente.setTelefone(rs.getString("telefone"));
                paciente.setEmail(rs.getString("email"));
                paciente.setObservacao(rs.getString("observacao"));

                lista.add(paciente);
            }
        } catch (Exception ex) {
            System.out.println("Erro ao realizar consulta");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão.....");
            }
        }
        return lista;
    }

    /*
    
    public static void main(String[] args) {
        PacienteDAO dao = new PacienteDAO();

        Paciente paciente = new Paciente();
        paciente.setNome("Pelé");
        paciente.setObservacao("Sou o jo soares sua piranha");

        dao.cadastrar(paciente);

        List<Paciente> lista = dao.listar();

        for (Paciente p : lista) {
            System.out.println(p.getId() + " " + p.getNome() + " " + p.getObservacao());
        }

        paciente.setNome("Garrincha");
        dao.cadastrar(paciente);

        lista = dao.listar();

        for (Paciente p : lista) {
            System.out.println(p.getId() + " " + p.getNome() + " " + p.getObservacao());
        }

    }
    
     */

 /*
    public static void main(String[] args) {

        PacienteDAO dao = new PacienteDAO();

        Paciente paciente = new Paciente();
        paciente.setId(2);
        dao.deletar(paciente);

        System.out.println(paciente.getId() + paciente.getNome());

    }
     */
    public static void main(String[] args) {
        PacienteDAO dao = new PacienteDAO();

        List<Paciente> lista = dao.listar();

        for (Paciente p : lista) {
            System.out.println(p.getId() + " " + p.getNome() + " " + p.getCidade());
        }
    }

}
