package br.com.senac.projetofinal.dao;

import br.com.senac.projetofinal.model.Doador;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DoadorDAO extends DAO<Doador> {

    @Override
    public void cadastrar(Doador doador) {
        Connection connection = null;

        try {
            String query;
            if (doador.getId() == 0) {

                query = "insert into doador (nome, cpf, sexo, dataNascimento, sangue, estado, cidade, telefone, email) values (?, ?, ?, ?, ?, ?, ?, ?, ?);";

            } else {

                query = "update doador set nome = ? , cpf = ? , sexo = ? , dataNascimento = ? , sangue = ? , estado = ? , cidade = ? , telefone = ? , email = ? where id = ? ;";

            }

            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, doador.getNome());
            ps.setString(2, doador.getCpf());
            ps.setString(3, doador.getSexo());
            ps.setString(4, doador.getDataNascimento());
            ps.setString(5, doador.getSangue());
            ps.setString(6, doador.getEstado());
            ps.setString(7, doador.getCidade());
            ps.setString(8, doador.getTelefone());
            ps.setString(9, doador.getEmail());

            if (doador.getId() == 0) {
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                doador.setId(rs.getInt(1));

            } else {
                ps.setInt(10, doador.getId());
                ps.executeUpdate();

            }

        } catch (Exception ex) {

        } finally {

            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão....");
            }

        }

    }

    @Override
    public void deletar(Doador doador) {

        String query = "delete from doador where id = ? ;";
        Connection connection = null;

        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, doador.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            System.out.println("Erro ao deletar registro....");

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {

                System.out.println("Falha ao fechar conexão...");
            }
        }

    }

    @Override
    public List<Doador> listar() {

        String query = "select * from doador;";
        List<Doador> lista = new ArrayList<>();
        Connection connection = null;
        try {
            connection = Conexao.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Doador doador = new Doador();

                doador.setId(rs.getInt("id"));
                doador.setNome(rs.getString("nome"));
                doador.setCpf(rs.getString("cpf"));
                doador.setSexo(rs.getString("sexo"));
                doador.setDataNascimento(rs.getString("dataNascimento"));
                doador.setSangue(rs.getString("sangue"));
                doador.setEstado(rs.getString("estado"));
                doador.setCidade(rs.getString("cidade"));
                doador.setTelefone(rs.getString("telefone"));
                doador.setEmail(rs.getString("email"));

                lista.add(doador);
            }
        } catch (Exception ex) {
            System.out.println("Ocorreu um erro ao fazer a consulta.....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão....");
            }
        }
        return lista;
    }

    @Override
    public Doador get(int id) {

        Doador doador = null;
        Connection connection = null;
        String query = "select * from doador where id = ? ; ";
        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {

                doador = new Doador();

                doador.setId(rs.getInt("id"));
                doador.setNome(rs.getString("nome"));
                doador.setCpf(rs.getString("cpf"));
                doador.setSexo(rs.getString("sexo"));
                doador.setDataNascimento(rs.getString("dataNascimento"));
                doador.setSangue(rs.getString("sangue"));
                doador.setEstado(rs.getString("estado"));
                doador.setCidade(rs.getString("cidade"));
                doador.setTelefone(rs.getString("telefone"));
                doador.setEmail(rs.getString("email"));
            }
        } catch (Exception ex) {
            System.out.println("Erro ao executar a consulta....");

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão....");
            }
        }
        return doador;
    }

    public List<Doador> getByFiltro(Integer id, String nome, String estado, String sangue) {

        List<Doador> lista = new ArrayList<>();
        Connection connection = null;

        try {

            StringBuilder sb = new StringBuilder("select * from doador where 1 = 1");

            if (id != null) {
                sb.append(" and id  = ? ");

            }

            if (nome != null && !nome.trim().isEmpty()) {
                sb.append(" and nome like ? ");

            }

            if (estado != null && !estado.trim().isEmpty()) {
                sb.append(" and estado like ? ");
            }

            if (sangue != null && !sangue.trim().isEmpty()) {
                sb.append(" and sangue like ? ");

            }

            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(sb.toString());
            int index = 0;
            if (id != null) {
                ps.setInt(++index, id);
            }

            if (nome != null && !nome.trim().isEmpty()) {
                ps.setString(++index, "%" + nome + "%");
            }
            if (estado != null && !estado.trim().isEmpty()) {
                ps.setString(++index, "%" + estado + "%");
            }

            if (sangue != null && !sangue.trim().isEmpty()) {
                ps.setString(++index, "%" + sangue + "%");
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Doador doador = new Doador();

                doador.setId(rs.getInt("id"));
                doador.setNome(rs.getString("nome"));
                doador.setCpf(rs.getString("cpf"));
                doador.setSexo(rs.getString("sexo"));
                doador.setDataNascimento(rs.getString("dataNascimento"));
                doador.setSangue(rs.getString("sangue"));
                doador.setEstado(rs.getString("estado"));
                doador.setCidade(rs.getString("cidade"));
                doador.setTelefone(rs.getString("telefone"));
                doador.setEmail(rs.getString("email"));

                lista.add(doador);

            }

        } catch (Exception ex) {
            System.out.println("Erro ao realizar consulta");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão.....");
            }
        }
        return lista;

    }

    /*
    public static void main(String[] args) {
        DoadorDAO dao = new DoadorDAO();

        Doador doador = new Doador();
        
        doador.setNome("CR7");
        doador.setCpf("123456789");
        doador.setCidade("turim");

        dao.cadastrar(doador);

         System.out.println(doador.getId() + " " + doador.getNome() + " " + doador.getCidade());
      
        List<Doador> lista = dao.listar();

        for (Doador d : lista) {
            System.out.println(d.getId() + " " + d.getNome() + " " + d.getCidade());
        }

        doador.setNome("Conaldo");
        dao.cadastrar(doador);

        lista = dao.listar();

        for (Doador d : lista) {
            System.out.println(d.getId() + " " + d.getNome() + " " + d.getCidade());
        } 

    }
     */
    public static void main(String[] args) {
        DoadorDAO dao = new DoadorDAO();

        List<Doador> lista = dao.listar();

        for (Doador d : lista) {
            System.out.println(d.getId() + " " + d.getNome() + " " + d.getCidade());
        }

    }

}
