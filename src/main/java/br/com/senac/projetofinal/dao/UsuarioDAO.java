package br.com.senac.projetofinal.dao;

import br.com.senac.projetofinal.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO extends DAO<Usuario> {

    @Override
    public void cadastrar(Usuario usuario) {
        Connection connection = null;
        try {

            String query;
            if (usuario.getId() == 0) {

                query = "insert into usuario (nome, sexo, dataNascimento, sangue, estado, cidade, bairro, endereco, telefone, email, senha) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            } else {
                query = "update usuario set nome = ? , sexo = ? , dataNascimento = ? , sangue = ? , estado = ? , cidade = ? , bairro = ? , endereco = ? , telefone = ? , email = ? , senha = ? where id = ? ;";
            }

            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, usuario.getNome());
            ps.setString(2, usuario.getSexo());
            ps.setString(3, usuario.getDataNascimento());
            ps.setString(4, usuario.getSangue());
            ps.setString(5, usuario.getEstado());
            ps.setString(6, usuario.getCidade());
            ps.setString(7, usuario.getBairro());
            ps.setString(8, usuario.getEndereco());
            ps.setString(9, usuario.getTelefone());
            ps.setString(10, usuario.getEmail());
            ps.setString(11, usuario.getSenha());
            if (usuario.getId() == 0) {
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                usuario.setId(rs.getInt(1));

            } else {
                ps.setInt(12, usuario.getId());
                ps.executeUpdate();
            }

        } catch (Exception ex) {

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão....");
            }
        }

    }

    @Override
    public void deletar(Usuario objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Usuario> listar() {

        String query = "select * from usuario;";
        List<Usuario> lista = new ArrayList<>();
        Connection connection = null;

        try {
            connection = Conexao.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Usuario usuario = new Usuario();

                usuario.setId(rs.getInt("id"));
                usuario.setNome(rs.getString("nome"));
                usuario.setSexo(rs.getString("sexo"));
                usuario.setDataNascimento(rs.getString("dataNascimento"));
                usuario.setSangue(rs.getString("sangue"));
                usuario.setEstado(rs.getString("estado"));
                usuario.setCidade(rs.getString("cidade"));
                usuario.setBairro(rs.getString("bairro"));
                usuario.setEndereco(rs.getString("endereco"));
                usuario.setTelefone(rs.getString("telefone"));
                usuario.setEmail(rs.getString("email"));
                usuario.setSenha(rs.getString("senha"));

                lista.add(usuario);
            }
        } catch (Exception ex) {
            System.out.println("Ocorreu um erro ao fazer a consulta....");

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("falha oa fechar conexão.....");
            }

        }
        return lista;
    }

    @Override
    public Usuario get(int id) {
        Usuario usuario = null;
        Connection connection = null;
        String query = "select * from usuario where id = ? ; ";
        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {
                usuario = new Usuario();
                usuario.setId(rs.getInt("id"));
                usuario.setNome(rs.getString("nome"));
                usuario.setSexo(rs.getString("sexo"));
                usuario.setDataNascimento(rs.getString("dataNascimento"));
                usuario.setSangue(rs.getString("sangue"));
                usuario.setEstado(rs.getString("estado"));
                usuario.setCidade(rs.getString("cidade"));
                usuario.setBairro(rs.getString("bairro"));
                usuario.setEndereco(rs.getString("endereco"));
                usuario.setTelefone(rs.getString("telefone"));
                usuario.setEmail(rs.getString("email"));
                usuario.setSenha(rs.getString("senha"));
                
            }

        } catch (Exception ex) {
            System.out.println("Erro ao executar a consulta....");

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão....");
            }
        }
        return usuario;
    }

    public Usuario getByname(String email) {

        Usuario usuario = null;
        Connection connection = null;
        String query = "select * from usuario where email = ? ; ";
        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {
                usuario = new Usuario();
                usuario.setId(rs.getInt("id"));
                usuario.setNome(rs.getString("nome"));
                usuario.setSexo(rs.getString("sexo"));
                usuario.setDataNascimento(rs.getString("dataNascimento"));
                usuario.setSangue(rs.getString("sangue"));
                usuario.setEstado(rs.getString("estado"));
                usuario.setCidade(rs.getString("cidade"));
                usuario.setBairro(rs.getString("bairro"));
                usuario.setEndereco(rs.getString("endereco"));
                usuario.setTelefone(rs.getString("telefone"));
                usuario.setEmail(rs.getString("email"));
                usuario.setSenha(rs.getString("senha"));
            }

        } catch (Exception ex) {
            System.out.println("Erro ao executar a consulta....");

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão....");
            }
        }
        return usuario;

    }

    /*
    public static void main(String[] args) {
        UsuarioDAO dao = new UsuarioDAO();
        
        Usuario usuario = new Usuario();
        usuario.setNome("Lionel Messi");
        usuario.setSenha("123");
        
        usuario.setTelefone("40028922");
        dao.cadastrar(usuario);
        
        System.out.println(usuario.getTelefone());
    }
     */
    public static void main(String[] args) {
        UsuarioDAO dao = new UsuarioDAO();

        List<Usuario> lista = dao.listar();

        for (Usuario u : lista) {
            System.out.println(u.getId() + " " + u.getNome() + " " + u.getCidade());
        }

    }
}
