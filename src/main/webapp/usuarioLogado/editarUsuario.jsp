
<%@page import="br.com.senac.projetofinal.model.Usuario" %>
<jsp:include page="cabecalho.jsp" />

<link href="../resources/css/cadastro.css" rel="stylesheet" type="text/css"/>


<%
    Usuario usuario = (Usuario) request.getAttribute("usuario");
    String mensagem = (String) request.getAttribute("mensagem");
    String erro = (String) request.getAttribute("erro");
%>



<form action="./EditarUsuarioServlet" method="post" class="form-cadastro">

    <input type="hidden" name="id" value="<%= usuario != null ? usuario.getId() : ""%>" />

    <table class="table-cadastro">

        <tr>

            <td>
                <div>
                    <center> 
                        <% if (mensagem != null) {%>
                        <div class="alert alert-success" role="alert">
                            <%= mensagem%>
                        </div>
                        <%}%>

                        <% if (erro != null) {%>
                        <div class="alert alert-danger" role="alert">
                            <%= erro%>
                        </div>
                        <%}%>

                        <h2 id="txtCadastro" >Cadastre-se</h2>
                        <img src="../resources/imagens/LOGO PRINCIPAL.png" width="20%"  alt=""/>

                    </center>
                </div>


            </td>

        </tr>
        <tr>

            <td>
                <h1 class="fonte-titulo"> Dados Pessoais: </h1>
                &nbsp; &nbsp;

                <img src="../resources/imagens/users.png" alt=""/> 
                <input class="campo-cadastro"  type="text"  name="nome"  placeholder="Digite seu Nome" size="30" required="" value="<%= usuario != null ? usuario.getNome() : ""%>"  />

                &nbsp; &nbsp;

                <img src="../resources/imagens/men-and-women.png" alt=""/>
                <select class="campo-cadastro" id="sexo" name="sexo"  required="" <%= usuario != null ? usuario.getSexo() : ""%> >
                    <option value="">Sexo</option>
                    <option  value="Masculino"> Masculino </option>
                    <option value="Feminino"> Feminino </option>
                </select>


                &nbsp; &nbsp;


                <img src="../resources/imagens/calendar.png" alt=""/>
                <input class="campo-cadastro" type="date" id="dataNascimento" name="dataNascimento" required=""  value="<%= usuario != null ? usuario.getDataNascimento() : ""%>" />


                <p></p>
                &nbsp; &nbsp;

                <img src="../resources/imagens/blood.png" alt=""/>
                <select class="campo-cadastro" id="sangue" name="sangue" required="" <%= usuario != null ? usuario.getSangue() : ""%> >
                    <option value=""> Tipos de Sangue</option>
                    <option  value="A-"> A- </option>
                    <option  value="A+"> A+ </option>
                    <option  value="B-"> B- </option>
                    <option  value="B+"> B+ </option>
                    <option  value="AB-"> AB- </option>
                    <option  value="AB+"> AB+ </option>
                    <option  value="O-"> O- </option>
                    <option  value="O+"> O+ </option>

                </select>
            </td>
        </tr>
        <tr>
            <td>
                <h1 class="fonte-titulo" > Localiza��o: </h1>
                &nbsp; &nbsp;
                <img src="../resources/imagens/map-location.png" alt=""/>
                <select class="campo-cadastro" id="estado" name="estado" required=""  <%= usuario != null ? usuario.getEstado() : ""%> >
                    <option value=""> Selecione o Estado </option>
                    <option value="AC">AC</option>
                    <option value="AL">AL </option>
                    <option value="AP">AP </option>
                    <option value="AM">AM </option>
                    <option value="BA">BA </option>
                    <option value="CE">CE </option>
                    <option value="DF">DF </option>
                    <option value="ES">ES </option>
                    <option value="GO">GO </option>
                    <option value="MA">MA </option>
                    <option value="MT">MT </option>
                    <option value="MS">MS </option>
                    <option value="MG">MG </option>
                    <option value="PA">PA </option>
                    <option value="PB">PB </option>
                    <option value="PR">PR </option>
                    <option value="PE">PE </option>
                    <option value="PI">PI </option>
                    <option value="RJ">RJ </option>
                    <option value="RN">RN </option>
                    <option value="RS">RS </option>
                    <option value="RO">RO </option>
                    <option value="RR">RR </option>
                    <option value="SC">SC </option>
                    <option value="SP">SP </option>
                    <option value="SE">SE </option>
                    <option value="TO">TO </option>
                </select>
                &nbsp; &nbsp;

                <img src="../resources/imagens/cityscape.png" alt=""/>
                <input class="campo-cadastro" type="text" id="cidade" name="cidade" placeholder="Digite sua cidade" size="30" required="" value="<%= usuario != null ? usuario.getCidade() : ""%>" >


                <p></p>
                &nbsp; &nbsp;
                <img src="../resources/imagens/placeholder.png" alt=""/>
                <input class="campo-cadastro" type="text" id="bairro" name="bairro" placeholder="Digite seu Bairro" size="30" required="" value="<%= usuario != null ? usuario.getBairro() : ""%>"  >



                &nbsp; &nbsp;
                <img src="../resources/imagens/compass.png" alt=""/>
                <input class="campo-cadastro" type="text" id="endereco" name="endereco" placeholder="Digite seu endere�o" size="30" required="" value="<%= usuario != null ? usuario.getEndereco() : ""%>" >

            </td>

        </tr>
        <tr>

            <td>

                <h1 class="fonte-titulo" > Dados de acesso e contato: </h1>

                &nbsp; &nbsp;
                <img src="../resources/imagens/smartphone-call.png" alt=""/>
                <input class="  campo-cadastro  " type="text" id="telefone" name="telefone"   placeholder="Digite seu telefone" maxlength="11" size="30" required="" value="<%= usuario != null ? usuario.getTelefone() : ""%>" />

                &nbsp; &nbsp;


                <img src="../resources/imagens/opened-email-envelope.png" alt=""/>
                <input class="campo-cadastro" type="email" id="email" name="email" placeholder="Digite seu email" size="30" required="" value="<%= usuario != null ? usuario.getEmail() : ""%>"  />



                <p></p>


                &nbsp; &nbsp;

                <img src="../resources/imagens/locked-padlock.png" alt=""/>
                <input class="campo-cadastro" type="password" id="senha" name="senha" placeholder="Digite sua senha" required="" value="<%= usuario != null ? usuario.getSenha() : ""%>"  /> 

            </td>
        </tr>
        <tr>



            <td colspan="2"   >

                <p class="fonte-cadastro">Ao clicar em "Cadastrar-me" informo que meus dados sao verdadeiros.</p>


                <p>
                    &nbsp; &nbsp;
                    <input type="submit" class="btn btn-success btn-lg" value="Cadastrar-me"   />
                    <input type="reset" class="btn btn-danger btn-lg" value="Cancelar"  >
                </p>
            </td>
        </tr>

    </table>

</form>



<jsp:include page="rodape.jsp"/>