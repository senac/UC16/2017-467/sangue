
<%@page import="br.com.senac.projetofinal.model.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>

        <title>Doe mais Saúde!!!</title>
        <link rel="shortcut icon" href="../resources/imagens/logo.ico" type="image/x-icon" />

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <link href="../resources/css/cabecalhoFixo.css" rel="stylesheet" type="text/css"/>
        <link href="../resources/css/fundo.css" rel="stylesheet" type="text/css"/>
    </head>
    <%

        Usuario usuario = (Usuario) session.getAttribute("user");


    %>

    <body>
        <nav class=" navbar-inverse  " >
            <div class=" container-fluid " >

                <ul class="nav navbar-nav">
                    <li>
                        <a href="homeLogado.jsp">                       
                            <img src="../resources/imagens/LOGO PRINCIPAL.png"  width="64" height="64" alt=""/>
                        </a>
                    </li>
                    <li class="fonte-tamanho"><a href="etapasDeDoacaoDeSangue.jsp">
                            <img src="../resources/imagens/donorMulher.png" alt=""/>
                            Etapas de doação de sangue
                        </a></li>
                    <li class="fonte-tamanho"><a href="mitosEverdades.jsp">
                            <img src="../resources/imagens/solidarity.png" alt=""/>
                            Mitos e verdades sobre doar sangue
                        </a></li>



                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class=" fonte-tamanho  dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img src="../resources/imagens/medical-history.png" alt=""/>
                            Cadastrar 
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="cadastrarDoador.jsp">
                                    <img src="../resources/imagens/doador.png" alt=""/>
                                    Cadastrar Doador
                                </a></li>
                            <hr/>
                            <li><a href="cadastrarPaciente.jsp">
                                    <img src="../resources/imagens/patient.png" alt=""/>
                                    Cadastra Paciente
                                </a></li>
                        </ul>
                    </li>
                    <li class=" fonte-tamanho dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img src="../resources/imagens/pesquisar.png" alt=""/>
                            Pesquisar 
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="pesquisarDoador.jsp">
                                    <img src="../resources/imagens/doador.png" alt=""/>
                                    Pesquisar Doador
                                </a></li>
                            <hr/>
                            <li><a href="pesquisarPaciente.jsp">
                                    <img src="../resources/imagens/patient.png" alt=""/>
                                    Pesquisar Paciente
                                </a></li>

                        </ul>
                    </li>

                    <li class="  fonte-tamanho dropdown  "><a class="dropdown-toggle" data-toggle="dropdown" href="#"> <img src="../resources/imagens/user.png" alt=""/>
                            <%= usuario.getNome()%>
                        </a>
                        <ul class="dropdown-menu">



                            <li><a href='./EditarUsuarioServlet?id=<%= usuario.getId()%>'  >
                                    <img src="../resources/imagens/edit.png" alt=""/>
                                    Editar Perfil
                                </a></li>
                            <hr/>
                            <li><a href="" data-toggle="modal" data-target="#modalSair"  >
                                    <img src="../resources/imagens/exit.png" alt=""/>
                                    Sair
                                </a></li>

                        </ul>

                    </li>

                </ul>

            </div>

        </nav>


        <div class="modal fade" id="modalSair" tabindex="-1" role="dialog" 
             aria-labelledby="exampleModalLabel2" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <center>
                            <h2 class="modal-title" id="exampleModalLabel2">Sair</h2>
                        </center>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="font-size: 20px;" >
                        <center >
                            <p> Você esta prestes a sair.</p> 
                            Quer mesmo sair
                            <span><%= usuario.getNome()%></span> ?
                        </center>
                    </div>
                    <div class="modal-footer" >
                        <form action="logout">
                            <center>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="font-size: 20px;" >Não</button>

                                <button id="btnSim" class="btn btn-primary" style="font-size: 20px;" >Sim </button>
                            </center>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="espaco"></div>