<%@page import="br.com.senac.projetofinal.model.Doador" %>
<%@page import="java.util.List" %>
<jsp:include page="cabecalho.jsp" />


<link href="../resources/css/pesquisar.css" rel="stylesheet" type="text/css"/>
<script src="../resources/jquery/deletarDoador.js" type="text/javascript"></script>


<% List<Doador> lista = (List) request.getAttribute("lista");%>
<% String mensagem = (String) request.getAttribute("mensagem");%>

<div class="bordaPesquisar">

    <% if (mensagem != null) {%>

    <div class="alert-danger" style="font-size: 35px" >
        <%= mensagem%>

    </div>
    <% }%>

    <fieldset class="bordaBaixo" >
        <legend>
            Pesquisar Doador:
        </legend>

        <form action="./PesquisarDoadorServlet">

            <div class="row">

                <div class="col-sm-6">
                    <label for="nome" style="margin-right: 10px; " >Nome: </label>
                    <input class="form-control form-control-sm" name="nome" id="txtNome" type="text" placeholder="Digite seu nome" >

                </div>
                <div class="col-sm-3">
                    <label for="estado" style="margin-right: 10px; " >Estado: </label>
                    <select class="form-control form-control-sm" name="estado" id="txtEstado"  >
                        <option value=""> Selecione o Estado </option>
                        <option value="AC">AC</option>
                        <option value="AL">AL </option>
                        <option value="AP">AP </option>
                        <option value="AM">AM </option>
                        <option value="BA">BA </option>
                        <option value="CE">CE </option>
                        <option value="DF">DF </option>
                        <option value="ES">ES </option>
                        <option value="GO">GO </option>
                        <option value="MA">MA </option>
                        <option value="MT">MT </option>
                        <option value="MS">MS </option>
                        <option value="MG">MG </option>
                        <option value="PA">PA </option>
                        <option value="PB">PB </option>
                        <option value="PR">PR </option>
                        <option value="PE">PE </option>
                        <option value="PI">PI </option>
                        <option value="RJ">RJ </option>
                        <option value="RN">RN </option>
                        <option value="RS">RS </option>
                        <option value="RO">RO </option>
                        <option value="RR">RR </option>
                        <option value="SC">SC </option>
                        <option value="SP">SP </option>
                        <option value="SE">SE </option>
                        <option value="TO">TO </option>
                    </select>

                </div>
                <div class="col-sm-3">
                    <label for="sangue" style="margin-right: 10px; " >Tipo sangu�neo: </label>
                    <select class="form-control form-control-sm" name="sangue" id="txtSangue" >
                        <option value=""> Tipos de Sangue</option>
                        <option  value="A-"> A- </option>
                        <option  value="A+"> A+ </option>
                        <option  value="B-"> B- </option>
                        <option  value="B+"> B+ </option>
                        <option  value="AB-"> AB- </option>
                        <option  value="AB+"> AB+ </option>
                        <option  value="O-"> O- </option>
                        <option  value="O+"> O+ </option>
                    </select>        
                </div>

            </div>

            <button style="margin-top: 10px;" type="submit" class="  btn btn-default"   >
                Pesquisar
            </button>
            <p></p>

        </form>


    </fieldset>


    <table class="table table-hover" >

        <thead >
            <tr>
                <th>Perfil</th> <th>Nome</th>  <th>Tipo de sangue</th> <th>Estado</th>
            </tr>
        </thead>

        <%
            if (lista != null && lista.size() > 0) {
                for (Doador d : lista) {

        %>

        <tr>
            <td> <a href='./VerPerfilDoadorServlet?id=<%= d.getId()%>'>
                    <img src="../resources/imagens/doadorCadastro.png" width="64" height="64" alt=""/>

                </a>
            </td>

            <td><%= d.getNome()%></td>  <td> <%= d.getSangue()%> </td> <td> <%= d.getEstado()%> </td>

            <td style="width: 110px;">
                <a href='./CadastrarDoadorServlet?id=<%= d.getId()%>'  style="margin-right: 40px;"  >
                    <img src="../resources/imagens/edit.png" width="24" height="24" alt=""/>
                </a>

                <a href="" data-toggle="modal" data-target="#modalExclusao" 
                   onclick="deletarDoador(<%= d.getId()%>, '<%= d.getNome()%>')" >
                    <img src="../resources/imagens/delete.png" alt=""/>
                </a>

            </td>

        </tr>

        <%  }
        } else {

        %>


        <tr>
            <td colspan="2">N�o existem registros.</td>

        </tr>

        <% }%>


    </table>



</div>

<div class="modal fade" id="modalExclusao" tabindex="-1" role="dialog" 
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <center>
                    <h2 class="modal-title" id="exampleModalLabel">Exclus�o</h2>
                </center>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="font-size: 20px;">
                Deseja realmente apagar o contato <span id="nomeDoador"></span> ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="font-size: 20px;" >Cancelar</button>

                <a  id="btnConfirmar" class="btn btn-primary" style="font-size: 20px;" >Confirmar</a>
            </div>
        </div>
    </div>
</div>


<jsp:include page="rodape.jsp"/>
