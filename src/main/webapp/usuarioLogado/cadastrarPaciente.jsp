
<%@page import="br.com.senac.projetofinal.model.Paciente" %>
<jsp:include page="cabecalho.jsp" />

<link href="../resources/css/cadastroPaciente.css" rel="stylesheet" type="text/css"/>
<link href="../resources/css/cadastro.css" rel="stylesheet" type="text/css"/>


<%

    Paciente paciente = (Paciente) request.getAttribute("paciente");
    String mensagem = (String) request.getAttribute("mensagem");
    String erro = (String) request.getAttribute("erro");

%>



<form action="./CadastrarPacienteServlet" method="post" class="form-cadastro">

    <input type="hidden" name="id" value="<%= paciente != null ? paciente.getId() : ""%>" />

    <table class="table-cadastro">

        <tr>

            <td>

                <div>
                    <center>
                        <% if (mensagem != null) {%>
                        <div class="alert alert-success" style="font-size: 35px" role="alert">
                            <%= mensagem%>
                        </div>
                        <%}%>

                        <% if (erro != null) {%>
                        <div class="alert alert-danger" style="font-size: 35px" role="alert">
                            <%= erro%>
                        </div>
                        <%}%>
                    </center>
                    <h1 class="bordaBottom fonte-titulo"><b>Precisa de sangue, ou conhece alguem que precise??</b> 
                        Fa�a o cadastro agora mesmo, para que outras pessoas possam ajudar.</h1>

                </div>

            </td>

        </tr>
        <tr>

            <td>
                <h1 class="fonte-titulo" > Dados Pessoais do paciente: </h1>
                &nbsp; &nbsp;

                <img src="../resources/imagens/patientCadastro.png" width="64" height="64" alt=""/>

                <input class="campo-cadastro"  type="text" id="nome" name="nome" placeholder="Digite o Nome" size="45" required="" value="<%= paciente != null ? paciente.getNome() : ""%>"  />



                &nbsp; &nbsp;

                <img src="../resources/imagens/id-card.png" alt=""/>
                <input class="campo-cadastro" type="text" id="cpf" name="cpf" placeholder="Digite o CPF" maxlength="11" size="15" required="" value="<%= paciente != null ? paciente.getCpf() : ""%>" >

                <p> </p>
                &nbsp; &nbsp; &nbsp;

                <img src="../resources/imagens/men-and-women.png" alt=""/>
                <select class="campo-cadastro" id="sexo" name="sexo"  required="" <%= paciente != null ? paciente.getSexo() : ""%> >
                    <option value="">Sexo</option>
                    <option  value="Masculino"> Masculino </option>
                    <option value="Feminino"> Feminino </option>
                </select>



                &nbsp; &nbsp; 
                <img src="../resources/imagens/cake.png" alt=""/>

                <input class="campo-cadastro" type="number"  id="idade" name="idade" min="1"  placeholder="Digite a idade" size="10" required="" value="<%= paciente != null ? paciente.getIdade() : ""%>"   />


                &nbsp; &nbsp; 

                <img src="../resources/imagens/blood.png" alt=""/>
                <select class="campo-cadastro" id="sangue" name="sangue" required="" <%= paciente != null ? paciente.getSangue() : ""%> >
                    <option value=""> Tipos de Sangue</option>
                    <option  value="A-"> A- </option>
                    <option  value="A+"> A+ </option>
                    <option  value="B-"> B- </option>
                    <option  value="B+"> B+ </option>
                    <option  value="AB-"> AB- </option>
                    <option  value="AB+"> AB+ </option>
                    <option  value="O-"> O- </option>
                    <option  value="O+"> O+ </option>

                </select>
            </td>
        </tr>
        <tr>
            <td>
                <h1 class="fonte-titulo" > Informa��o para doa��o: </h1>


                &nbsp; &nbsp;
                <p></p>
                &nbsp; &nbsp;

                <img src="../resources/imagens/calendar.png" alt=""/>
                <input class="campo-cadastro" type="date" id="dataLimite" name="dataLimite"  required="" value="<%= paciente != null ? paciente.getDataLimite() : ""%>"  >
                <b class="fonte-cadastro">Data limite para encontrar doadores compativeis.</b>
                &nbsp; &nbsp;

                <p> </p>
                &nbsp; &nbsp;

                <img src="../resources/imagens/hospital.png" alt=""/>

                <input class="campo-cadastro" type="text" id="hospital" name="hospital" size="28" placeholder="Centro de doa��o" required="" value="<%= paciente != null ? paciente.getHospital() : ""%>" >
                <b class="fonte-cadastro">Hospital/unidade onde as pessoas devem ir para doar.</b>
                <p></p>
                &nbsp; &nbsp;
                <img src="../resources/imagens/map-location.png" alt=""/>
                <select class="campo-cadastro" id="estado" name="estado" required=""  <%= paciente != null ? paciente.getEstado() : ""%> >
                    <option value=""> Selecione o Estado </option>
                    <option value="AC">AC</option>
                    <option value="AL">AL </option>
                    <option value="AP">AP </option>
                    <option value="AM">AM </option>
                    <option value="BA">BA </option>
                    <option value="CE">CE </option>
                    <option value="DF">DF </option>
                    <option value="ES">ES </option>
                    <option value="GO">GO </option>
                    <option value="MA">MA </option>
                    <option value="MT">MT </option>
                    <option value="MS">MS </option>
                    <option value="MG">MG </option>
                    <option value="PA">PA </option>
                    <option value="PB">PB </option>
                    <option value="PR">PR </option>
                    <option value="PE">PE </option>
                    <option value="PI">PI </option>
                    <option value="RJ">RJ </option>
                    <option value="RN">RN </option>
                    <option value="RS">RS </option>
                    <option value="RO">RO </option>
                    <option value="RR">RR </option>
                    <option value="SC">SC </option>
                    <option value="SP">SP </option>
                    <option value="SE">SE </option>
                    <option value="TO">TO </option>
                </select>
                &nbsp; &nbsp;

                <img src="../resources/imagens/cityscape.png" alt=""/>
                <input class="campo-cadastro" type="text" id="cidade" name="cidade" placeholder="Digite a cidade" size="30" required="" value="<%= paciente != null ? paciente.getCidade() : ""%>" >


            </td>

        </tr>

        <tr>

            <td>

                <h1 class="fonte-titulo"> Dados para contato: </h1>

                &nbsp; &nbsp;
                <img src="../resources/imagens/smartphone-call.png" alt=""/>
                <input class="  campo-cadastro  " type="text" id="telefone"  name="telefone" maxlength="11"  placeholder="Digite o telefone" size="30" required="" value="<%= paciente != null ? paciente.getTelefone() : ""%>" />

                &nbsp; &nbsp;


                <img src="../resources/imagens/opened-email-envelope.png" alt=""/>
                <input class="campo-cadastro" type="email" id="email" name="email" placeholder="Digite o email" size="30" required="" value="<%= paciente != null ? paciente.getEmail() : ""%>"  />

            </td>
        </tr>

        <tr>
            <td>
                <h1 class="fonte-titulo"> Observa��es: </h1>
                &nbsp; &nbsp;
                <textarea class="campo-cadastro tamanhoTextArea" id="observacao" name="observacao" placeholder="Coloque aqui alguma informa��o do paciente que n�o foi listado" ><%= paciente != null ? paciente.getObservacao() : ""%></textarea>
            </td>
        </tr>


        <tr>



            <td colspan="2"   >

                <p class="fonte-cadastro">Ao clicar em "Cadastrar" informo que os dados acima sao verdadeiros e que tambem estou apto a ser doador.</p>


                <p>
                    &nbsp; &nbsp;
                    <input type="submit" class="btn btn-success btn-lg" value="Cadastrar"  />
                    <input type="reset"  class="btn btn-danger btn-lg" value="Cancelar"  >
                </p>
            </td>
        </tr>

    </table>

</form>


<jsp:include page="rodape.jsp"/>