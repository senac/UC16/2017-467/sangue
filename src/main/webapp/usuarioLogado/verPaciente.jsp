
<%@page import="br.com.senac.projetofinal.model.Paciente" %>
<jsp:include page="cabecalho.jsp" />


<link href="../resources/css/cadastroPaciente.css" rel="stylesheet" type="text/css"/>
<link href="../resources/css/cadastro.css" rel="stylesheet" type="text/css"/>

<%
    Paciente paciente = (Paciente) request.getAttribute("paciente");
%>




<table class="table-cadastro" >


    <tr>

        <td  >
            <input type="hidden" name="id" value="<%= paciente.getId()%>" />
            <h1 class="fonte-titulo" > Dados Pessoais do paciente: <%= paciente.getNome()%> </h1>
            &nbsp; &nbsp;

            <img src="../resources/imagens/patientCadastro.png" width="64" height="64" alt=""/>

            <input class="campo-cadastro"  type="text" id="nome" name="nome" size="50" value="<%= paciente.getNome()%>"  readonly=""  />

            <p> </p>
            &nbsp; &nbsp; &nbsp;

            <img src="../resources/imagens/men-and-women.png" alt=""/>
            <input class="campo-cadastro"  type="text" id="sexo" name="sexo" size="6"  value="<%= paciente.getSexo()%>" readonly=""  />

            &nbsp; &nbsp; 
            <img src="../resources/imagens/cake.png" alt=""/>

            <input class="campo-cadastro" type="text"  id="idade" name="idade" size="2" value="<%= paciente.getIdade()%>" readonly=""  />
 
            

            &nbsp; &nbsp; 

            <img src="../resources/imagens/blood.png" alt=""/>


            <input class="campo-cadastro"  type="text" id="sangue" name="sangue" size="2" value="<%= paciente.getSangue()%>" readonly=""  />

        </td>
    </tr>
    <tr>
        <td>
            <h1 class="fonte-titulo" > Informa��o para doa��o: </h1>


            &nbsp; &nbsp;
            <p></p>
            &nbsp; &nbsp;

            <img src="../resources/imagens/calendar.png" alt=""/>
            <input class="campo-cadastro" type="date" id="dataLimite" name="dataLimite"  value="<%= paciente.getDataLimite()%>" readonly="" >
            <b class="fonte-cadastro">Data limite para encontrar doadores compativeis.</b>
            &nbsp; &nbsp;

            <p> </p>
            &nbsp; &nbsp;

            <img src="../resources/imagens/hospital.png" alt=""/>

            <input class="campo-cadastro" type="text" id="hospital" name="hospital" size="30"  value="<%= paciente.getHospital()%> " readonly="" >
            <b class="fonte-cadastro">Hospital/unidade onde as pessoas devem ir para doar.</b>
            <p></p>
            &nbsp; &nbsp;
            <img src="../resources/imagens/map-location.png" alt=""/>

            <input class="campo-cadastro"  type="text" id="estado" name="estado" size="2" value="<%= paciente.getEstado()%>" readonly=""  />


            &nbsp; &nbsp;

            <img src="../resources/imagens/cityscape.png" alt=""/>
            <input class="campo-cadastro" type="text" id="cidade" name="cidade"  size="30"  value="<%= paciente.getCidade()%>" readonly="" >


        </td>

    </tr>

    <tr>

        <td>

            <h1 class="fonte-titulo"> Dados para contato: </h1>

            &nbsp; &nbsp;
            <img src="../resources/imagens/smartphone-call.png" alt=""/>
            <input class="  campo-cadastro  " type="text" id="telefone"  name="telefone"  size="15" required="" value="<%= paciente.getTelefone()%>" readonly=""/>

            &nbsp; &nbsp;


            <img src="../resources/imagens/opened-email-envelope.png" alt=""/>
            <input class="campo-cadastro" type="email" id="email" name="email" size="30" value="<%= paciente.getEmail()%>" readonly=""  />

        </td>
    </tr>

    <tr>
        <td>
            <h1 class="fonte-titulo"> Observa��es: </h1>
            &nbsp; &nbsp;
            <textarea class="campo-cadastro tamanhoTextArea" id="observacao" name="observacao" readonly="" > <%= paciente.getObservacao()%> </textarea>
        </td>
    </tr>


    <tr>



        <td colspan="2"   >

            <p class="fonte-cadastro">Ao clicar em "Cadastrar" informo que os dados acima sao verdadeiros e que tambem estou apto a ser doador.</p>

            <form action="pesquisarPaciente.jsp">
                <p>
                    &nbsp; &nbsp;
                    <input type="submit" class="btn btn-success btn-lg" value="Voltar"  />

                </p>
            </form>
        </td>
    </tr>

</table>

<jsp:include page="rodape.jsp"/>