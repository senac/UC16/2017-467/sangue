<jsp:include page="cabecalho.jsp" />

<link href="../resources/css/centralizarTexto.css" rel="stylesheet" type="text/css"/>
<link href="../resources/css/doarSangue.css" rel="stylesheet" type="text/css"/>

<center>
    <div class="borda" >


        <h1 class="fonte-mitos" > Muitas pessoas tem a vontade de doar sangue, mais muitas delas n�o sabem quais s�o as etapas da doa��o.
            Colocaremos aqui quais s�o as etapas na hora de doar sangue.

        </h1>

        <img class="centralizar" src="../resources/imagens/etapasdoarsangue.png"  width="90%"  alt=""/>

        <h1> Etapas na hora de doar Sangue:  </h1>

        <h1> 1� Passo - Recep��o e cadastro</h1>

        <li class="fonte-etapas"> Apresenta��o de um documento oficial com foto; </li>

        <li class="fonte-etapas"> Atualiza��o de dados cadastrais; </li>
        <li class="fonte-etapas"> Antes de doar fa�a a ingest�o de dois copos de �gua. </li>

        <img src="../resources/imagens/cadastroEtapas.png" alt=""/>

        <h1> 2� Passo - Comunica��o Social</h1>
        <li class="fonte-etapas" > Orienta��es preliminares ao doador de sangue; </li>
        <li class="fonte-etapas" > Cadastro para doa��o de medula �ssea. </li>
        <img src="../resources/imagens/etapa2.png" alt=""/>

        <h1> 3� Passo - Pr�-triagem </h1>
        <li class="fonte-etapas" > Verifica��o de peso, altura, press�o arterial, pulso e temperatura;</li>
        <li class="fonte-etapas" > Teste de anemia. </li>
        <img src="../resources/imagens/etap3.png" alt=""/>

        <h1> 4� Passo - Entrevista cl�nica  </h1>
        <li class="fonte-etapas" > � confidencial e o sigilo � absoluto. Confie em seu entrevistador e seja sincero.</li>
        <img src="../resources/imagens/etapa4.png" alt=""/>

        <h1> 5� Passo - Coleta de sangue </h1>

        <li class="fonte-etapas" > A quantidade coletada � de 450 a 500 ml. S�o 8ml/Kg para mulher e 9ml/Kg pra homem; </li>
        <li class="fonte-etapas" > S�o coletadas amostras para teste de doen�as contagiosas e para verificar tipo sangu�neo. </li>
        <img src="../resources/imagens/etapa5.png" alt=""/>

        <h1> 6� Passo - Lanche </h1>
        <li class="fonte-etapas" > � acompanhado por l�quidos, para repor o volume retirado na doa��o </li>
        <img src="../resources/imagens/etapa6.png" alt=""/>

    </div>
</center>

<jsp:include page="rodape.jsp"/>