
<%@page import="br.com.senac.projetofinal.model.Doador" %>
<jsp:include page="cabecalho.jsp" />

<link href="../resources/css/cadastroDoador.css" rel="stylesheet" type="text/css"/>
<link href="../resources/css/cadastro.css" rel="stylesheet" type="text/css"/>



<%
    Doador doador = (Doador) request.getAttribute("doador");
    String mensagem = (String) request.getAttribute("mensagem");
    String erro = (String) request.getAttribute("erro");

%>



<form action="./CadastrarDoadorServlet" method="post" class="form-cadastro">

    <input type="hidden" name="id" value="<%= doador != null ? doador.getId() : ""%>" />
    <table class="table-cadastro">

        <tr>

            <td>
        <center> 

            <div>
                <% if (mensagem != null) {%>
                <div class="alert alert-success" style="font-size: 35px" role="alert">
                    <%= mensagem%>
                </div>
                <%}%>

                <% if (erro != null) {%>
                <div class="alert alert-danger" style="font-size: 35px" role="alert">
                    <%= erro%>
                </div>
                <%}%>

                <h1 class="bordaBottom"><img src="../resources/imagens/LOGO PRINCIPAL.png" width="80" height="80" alt=""/>
                    <b>Doe sangue!!!</b> Sua atitude pode salvar vidas</h1>

            </div>

        </center>
        </td>

        </tr>
        <tr>

            <td>

                <h1 class="fonte-titulo"> Dados Pessoais do doador: </h1>
                &nbsp; &nbsp;
                <img src="../resources/imagens/doadorCadastro.png"  width="64" height="64"  alt=""/>
                <input class="campo-cadastro"  type="text" id="nome" name="nome" placeholder="Digite o Nome"  size="30" required="" value="<%= doador != null ? doador.getNome() : ""%>"  />



                &nbsp; &nbsp;

                <img src="../resources/imagens/id-card.png" alt=""/>
                <input class="campo-cadastro" type="text" id="cpf" name="cpf" placeholder="Digite o CPF"  maxlength="11"  size="30" required="" value="<%= doador != null ? doador.getCpf() : ""%>"  >

                <p></p>
                &nbsp; &nbsp;

                <img src="../resources/imagens/men-and-women.png" alt=""/>
                <select class="campo-cadastro" id="sexo" name="sexo"  required="" <%= doador != null ? doador.getSexo() : ""%> >
                    <option value="">Sexo</option>
                    <option  value="Masculino"> Masculino </option>
                    <option value="Feminino"> Feminino </option>
                </select>

                &nbsp; &nbsp;


                <img src="../resources/imagens/calendar.png" alt=""/>
                <input class="campo-cadastro" type="date" id="dataNascimento" name="dataNascimento" required="" value="<%= doador != null ? doador.getDataNascimento() : ""%>"  />


                &nbsp; &nbsp;

                <img src="../resources/imagens/blood.png" alt=""/>
                <select class="campo-cadastro" id="sangue" name="sangue" required="" <%= doador != null ? doador.getSangue() : ""%> >
                    <option value=""> Tipos de Sangue</option>
                    <option  value="A-"> A- </option>
                    <option  value="A+"> A+ </option>
                    <option  value="B-"> B- </option>
                    <option  value="B+"> B+ </option>
                    <option  value="AB-"> AB- </option>
                    <option  value="AB+"> AB+ </option>
                    <option  value="O-"> O- </option>
                    <option  value="O+"> O+ </option>

                </select>
            </td>
        </tr>
        <tr>
            <td>
                <h1 class="fonte-titulo" > Localização: </h1>
                &nbsp; &nbsp;
                <img src="../resources/imagens/map-location.png" alt=""/>
                <select class="campo-cadastro" id="estado" name="estado" required=""  <%= doador != null ? doador.getEstado() : ""%> >
                    <option value=""> Selecione o Estado </option>
                    <option value="AC">AC</option>
                    <option value="AL">AL </option>
                    <option value="AP">AP </option>
                    <option value="AM">AM </option>
                    <option value="BA">BA </option>
                    <option value="CE">CE </option>
                    <option value="DF">DF </option>
                    <option value="ES">ES </option>
                    <option value="GO">GO </option>
                    <option value="MA">MA </option>
                    <option value="MT">MT </option>
                    <option value="MS">MS </option>
                    <option value="MG">MG </option>
                    <option value="PA">PA </option>
                    <option value="PB">PB </option>
                    <option value="PR">PR </option>
                    <option value="PE">PE </option>
                    <option value="PI">PI </option>
                    <option value="RJ">RJ </option>
                    <option value="RN">RN </option>
                    <option value="RS">RS </option>
                    <option value="RO">RO </option>
                    <option value="RR">RR </option>
                    <option value="SC">SC </option>
                    <option value="SP">SP </option>
                    <option value="SE">SE </option>
                    <option value="TO">TO </option>
                </select>

                &nbsp; &nbsp;

                <img src="../resources/imagens/cityscape.png" alt=""/>
                <input class="campo-cadastro" type="text" id="cidade" name="cidade" placeholder="Digite a cidade" size="30" required="" value="<%= doador != null ? doador.getCidade() : ""%>" >


            </td>

        </tr>
        <tr>

            <td>

                <h1 class="fonte-titulo" > Dados para contato: </h1>

                &nbsp; &nbsp;
                <img src="../resources/imagens/smartphone-call.png" alt=""/>
                <input class="  campo-cadastro  " type="text" id="telefone" name="telefone" maxlength="11"  placeholder="Digite o telefone" size="30" required="" value="<%= doador != null ? doador.getTelefone() : ""%>" />

                &nbsp; &nbsp;


                <img src="../resources/imagens/opened-email-envelope.png" alt=""/>
                <input class="campo-cadastro" type="email" id="email" name="email" placeholder="Digite o email" size="30" required="" value="<%= doador != null ? doador.getEmail() : ""%>"  />

            </td>
        </tr>

        <tr>



            <td colspan="2"   >

                <p class="fonte-cadastro">Ao clicar em "Cadastrar" informo que os dados acima sao verdadeiros e que tambem estou apto a ser doador.</p>


                <p>
                    &nbsp; &nbsp;
                    <input type="submit" class="btn btn-success btn-lg" value="Cadastrar"  />
                    <input type="reset" class="btn btn-danger btn-lg" value="Cancelar"  >
                </p>
            </td>
        </tr>

    </table>

</form>



<jsp:include page="rodape.jsp"/>