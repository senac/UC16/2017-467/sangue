<jsp:include page="cabecalho.jsp" />

<link href="../resources/css/centralizarTexto.css" rel="stylesheet" type="text/css"/>
<link href="../resources/css/doarSangue.css" rel="stylesheet" type="text/css"/>

<div class=" borda ">


    <h1 class="fonte-mitos" >Esclare�a 5 mitos e verdades sobre a doa��o de sangue.</h1>
    <h2 class="fonte-color"> Este simples ato solid�rio n�o faz mal � sa�de
        de quem doa e pode salvar at� quatro vidas </h2>
    <img class="centralizar" src="../resources/imagens/cr7DoandoSangue.jpg" width="90%"   alt="Cristiano Ronaldo doando sangue"/>
    <p></p>


    <p class="fonte-p"> Doar sangue � um dos atos mais humanit�rios que existe no mundo e, embora seja t�o simples,
        faz bastante diferen�a na vida de muitas pessoas. Doando sangue voc� pode ajudar salvar centenas de vidas
        e se tornar mais um her�i para muitos. Por�m algumas pessoas possuem muitas d�vidas sobre diversas coisas
        que ouvem a respeito da doa��o de sangue, j� que n�o sabem se � verdade ou se tudo n�o passa de
        um mero mito. </p>
    <p class="fonte-p">  Veja abaixo os mitos e verdades sobre a doa��o de sangue e veja que fazer o bem pode � t�o dif�cil quanto voc� pensava.  </p>

    <ol start="1" >

        <li class="fonte-li"> Tem que estar em jejum para poder doar sangue  </li>
        <p class="fonte-p" >  Isso n�o passa de um <b>mito</b>. Na realidade voc� deve estar muito bem alimentado e de prefer�ncia consumir alimentos ricos em vitaminas 
            para que voc� possa enriquecer o seu sangue com subst�ncias saud�veis. 
            Antes de doar, fa�a uma boa alimenta��o, consumindo cenoura, beterraba, tomate, alface e outros alimentos saud�veis. 
            Voc� tamb�m pode beber bastante �gua para que o seu sangue possa aumentar mais na hora de doar. </p>
        <img class="centralizar" src="../resources/imagens/jejum.png"  width="60%"  alt=""/>

        <li class="fonte-li">Pode doar sangue todo os meses </li>
        <p class="fonte-p"> <b>N�o se pode</b> doar sangue todo m�s porque isso pode ser prejudicial � sua sa�de. 
            O ideal � fazer a doa��o e dar um intervalo de dois meses para os homens, j� as mulheres devem esperar 90 dias para doar novamente. 
            Dessa forma o seu organismo poder� se recuperar pela solidariedade que voc� fez. </p>
        <img class="centralizar" src="../resources/imagens/calendario.gif" width="80%"  alt=""/>

        <li class="fonte-li"> O doador pode transmitir doen�as </li>
        <p class="fonte-p"> Sim, isso � <b>verdade</b>. Se o sangue n�o for bem avaliado pelos especialistas, 
            as pessoas que receberem o sangue de um doador que tenha algum tipo de doen�a passada atrav�s do sangue poder�o se contaminar. 
            Por isso, � necess�rio que doador seja muito sincero ao dizer se possui ou n�o alguma doen�a e, se caso ele n�o souber de sua enfermidade, 
            o sangue coletado sempre passa por an�lises que verificam se est� saud�vel para ser doado ou n�o. </p>
        <img class="centralizar" src="../resources/imagens/sangueMitosEverdades.jpg"  width="90%" alt=""/>

        <li class="fonte-li"> Diab�ticos n�o podem fazer doa��o de sangue  </li>
        <p class="fonte-p"> Isso � um fator que <b>depende</b> muito do doador. Se a diabetes estiver controlada com uma alimenta��o saud�vel e n�o apresentar altera��es vasculares, 
            pode ser doado o sangue sem problema algum. Caso o diab�tico fa�a o uso de insulina, ele n�o poder� doar. </p>
        <img  class="centralizar" src="../resources/imagens/diabete.jpg"  width="80%" alt=""/>

        <li class="fonte-li"> Quem tem tatuagens e piercings n�o pode doar sangue  </li>
        <p class="fonte-p"> Isso � um <b> mito </b>. Quem tem piercings e tatuagens podem doar sangue sim, 
            mas precisam esperar algum tempo ap�s ter feito os tais procedimentos para n�o correr o risco de doar sangue contaminado. 
            Essa espera dura cerca de seis meses at� um ano, pois esses procedimentos, de furar a pele e de utilizar agulhas para fixar a tatuagem, 
            podem causar transmiss�o de doen�as. Passado o tempo necess�rio, a pessoa pode doar quando ela quiser. </p>
        <img class="centralizar"  src="../resources/imagens/ibrahimovic tatoo.jpg"  width="80%"  alt=""/>   
    </ol>
</div>
<jsp:include page="rodape.jsp"/>