

<%@page import="br.com.senac.projetofinal.model.Doador" %>
<jsp:include page="cabecalho.jsp" />


<link href="../resources/css/cadastroDoador.css" rel="stylesheet" type="text/css"/>
<link href="../resources/css/cadastro.css" rel="stylesheet" type="text/css"/>



<%
    Doador doador = (Doador) request.getAttribute("doador");
%>





<table class="table-cadastro">

    <tr>

        <td>
            <input type="hidden" name="id" value="<%= doador.getId()%>" readonly="" />

            <h1 class="fonte-titulo"> Dados Pessoais do doador: <%= doador.getNome()%></h1>
            &nbsp; &nbsp;
            <img src="../resources/imagens/doadorCadastro.png"  width="64" height="64"  alt=""/>
            <input class="campo-cadastro"  type="text" id="nome" name="nome" size="50"  value=" <%= doador.getNome()%>" readonly="" />
            <p></p>
            &nbsp; &nbsp;

            <img src="../resources/imagens/men-and-women.png" alt=""/>
            <input class="campo-cadastro"  type="text" id="sexo" name="sexo" size="6"  value=" <%= doador.getSexo() %>" readonly="">

            &nbsp; &nbsp;


            <img src="../resources/imagens/calendar.png" alt=""/>
            <input class="campo-cadastro" type="date" id="dataNascimento" name="dataNascimento"  value="<%= doador.getDataNascimento()%>" readonly="" />


            &nbsp; &nbsp;

            <img src="../resources/imagens/blood.png" alt=""/>
            <input class="campo-cadastro"  type="text" id="sangue" name="sangue" size="2" value="<%= doador.getSangue()%>"  readonly="" >

        </td>
    </tr>
    <tr>
        <td>
            <h1 class="fonte-titulo" > Localização: </h1>
            &nbsp; &nbsp;
            <img src="../resources/imagens/map-location.png" alt=""/>
            <input class="campo-cadastro"  type="text" id="estado" name="estado" size="2"  value="<%= doador.getEstado()%>" readonly="" >


            &nbsp; &nbsp;

            <img src="../resources/imagens/cityscape.png" alt=""/>
            <input class="campo-cadastro" type="text" id="cidade" name="cidade" size="30" value="<%= doador.getCidade()%>" readonly="" >


        </td>

    </tr>
    <tr>

        <td>

            <h1 class="fonte-titulo" > Dados para contato: </h1>

            &nbsp; &nbsp;
            <img src="../resources/imagens/smartphone-call.png" alt=""/>
            <input class="  campo-cadastro  " type="text" id="telefone" name="telefone"  size="15" required="" value="<%= doador.getTelefone()%>" readonly=""  />

            &nbsp; &nbsp;


            <img src="../resources/imagens/opened-email-envelope.png" alt=""/>
            <input class="campo-cadastro" type="email" id="email" name="email" size="30" required="" value="<%= doador != null ? doador.getEmail() : ""%>" readonly="" />

        </td>
    </tr>

    <tr>



        <td colspan="2"   >

            <p class="fonte-cadastro">Ao clicar em "Cadastrar" informo que os dados acima sao verdadeiros e que tambem estou apto a ser doador.</p>

            <form action="pesquisarDoador.jsp">
                <p>
                    &nbsp; &nbsp;
                    <input type="submit" class="btn btn-success btn-lg" value="Voltar"   />

                </p>
            </form>
        </td>
    </tr>

</table>


<jsp:include page="rodape.jsp"/>