function deletarDoador(id, nome) {
    $('#nomeDoador').text(nome);

    var formNome = $('#txtNome').val();
    var formEstado = $('#txtEstado').val();

    var url = './DeletarDoadorServlet?id=' + id + "&formNome=" + formNome + "&formEstado=" + formEstado;

    $('#btnConfirmar').attr('href', url);

}