
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<html>
    <head>

        <title>Doe mais Saúde!!!</title>
        <link rel="shortcut icon" href="resources/imagens/logo.ico" type="image/x-icon" />

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <link href="resources/css/cabecalhoFixo.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/fundo.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <nav class=" navbar-inverse  " >
            <div class="  container-fluid " >

                <ul class="nav navbar-nav">
                    <li>
                        <a href="home.jsp">                       
                            <img src="resources/imagens/LOGO PRINCIPAL.png" width="64" height="64" alt=""/>
                        </a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="fonte-tamanho"><a href="cadastroUsuario.jsp">
                            <img src="resources/imagens/medical-history.png" alt=""/>
                            Cadastre-se
                        </a></li>
                    <li class="fonte-tamanho"><a href="login.jsp">
                            <img src="resources/imagens/login.png" alt=""/>
                            Login
                        </a></li>
                </ul>

            </div>

        </nav>
        <div class="espaco"></div>